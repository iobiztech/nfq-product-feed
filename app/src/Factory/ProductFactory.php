<?php

declare(strict_types=1);

namespace App\Factory;

use App\Entity\Category;
use App\Entity\Product;

class ProductFactory
{
    /**
     * @param int           $uniqueId
     * @param string|null   $name
     * @param string|null   $description
     * @param Category|null $category
     *
     * @return Product
     */
    public static function create(
        int $uniqueId,
        ?string $name = null,
        ?string $description = null,
        ?Category $category = null
    ): Product {
        $product = (new Product())
            ->setName(empty($name) ? null : $name)
            ->setUniqueId($uniqueId)
            ->setDescription(empty($description) ? null : $description)
        ;

        if (!is_null($category)) {
            $category->addProduct($product);
        }

        return $product;
    }
}