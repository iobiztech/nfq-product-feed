<?php

declare(strict_types=1);

namespace App\Factory;

use App\Entity\Category;

class CategoryFactory
{
    /**
     * @param string $name
     *
     * @return Category|null
     */
    public static function create(string $name): ?Category
    {
        if (empty($name)) {
            return null;
        }

        return (new Category())
            ->setName($name)
        ;
    }
}