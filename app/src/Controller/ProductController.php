<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Category;
use App\Entity\ProductImportFile;
use App\Form\ProductImportType;
use App\Message\ImportProductsMessage;
use App\Repository\CategoryRepository;
use App\Repository\ProductImportFileRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    public const CATEGORIES_PER_PAGE = 15;
    public const PRODUCTS_PER_PAGE = 15;

    /**
     * @Route(name="product_import", methods={"GET", "POST"})
     *
     * @param Request                     $request
     * @param ProductImportFileRepository $repository
     * @param MessageBusInterface         $messageBus
     *
     * @return Response
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function importAction(
        Request $request,
        ProductImportFileRepository $repository,
        MessageBusInterface $messageBus
    ): Response {
        $file = new ProductImportFile();
        $form = $this->createForm(ProductImportType::class, $file);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $repository->create($file);

            $messageBus->dispatch(new ImportProductsMessage($file->getId()));
            $this->addFlash('success', 'alert.product.import.queued');

            return $this->redirectToRoute('product_import');
        }

        return $this->render('product/import.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/categories", name="product_categories", methods={"GET"})
     *
     * @param Request            $request
     * @param PaginatorInterface $paginator
     * @param CategoryRepository $categoryRepository
     *
     * @return Response
     */
    public function categoriesAction(
        Request $request,
        PaginatorInterface $paginator,
        CategoryRepository $categoryRepository
    ): Response {
        $pagination = $paginator->paginate(
            $categoryRepository->findAllCategoriesQuery(),
            $request->query->getInt('page', 1),
            self::CATEGORIES_PER_PAGE
        );

        return $this->render('product/categories.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/categories/{name}", name="product_category_products", methods={"GET"})
     *
     * @param Request            $request
     * @param Category           $category
     * @param PaginatorInterface $paginator
     * @param ProductRepository  $productRepository
     *
     * @return Response
     */
    public function categoryProductsAction(
        Request $request,
        Category $category,
        PaginatorInterface $paginator,
        ProductRepository $productRepository
    ): Response {
        $pagination = $paginator->paginate(
            $productRepository->findProductsByCategoryQuery($category),
            $request->query->getInt('page', 1),
            self::PRODUCTS_PER_PAGE
        );

        return $this->render('product/categories_products.html.twig', [
            'pagination' => $pagination,
            'category' => $category,
        ]);
    }
}
