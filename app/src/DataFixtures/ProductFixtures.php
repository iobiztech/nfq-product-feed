<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\DataFixtures\Data\ProductData;
use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ProductFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $data = ProductData::getData();

        foreach ($data as $item) {
            $product = new Product();
            $product->setName($item['name']);
            $product->setDescription($item['description']);
            $product->setCategory($item['category']);
            $product->setUniqueId($item['uniqueId']);

            $manager->persist($product);
        }

        $manager->flush();
    }
}
