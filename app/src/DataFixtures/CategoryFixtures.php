<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\DataFixtures\Data\CategoryData;
use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $data = CategoryData::getData();

        foreach ($data as $item) {
            $category = new Category();
            $category->setName($item['name']);

            $manager->persist($category);
        }

        $manager->flush();
    }
}
