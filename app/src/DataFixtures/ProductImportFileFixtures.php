<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\DataFixtures\Data\ProductImportFileData;
use App\Entity\ProductImportFile;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ProductImportFileFixtures extends Fixture
{
    private string $projectDir;

    /**
     * ProductImportFileFixtures constructor.
     *
     * @param string $projectDir
     */
    public function __construct(string $projectDir)
    {
        $this->projectDir = $projectDir;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $data = ProductImportFileData::getData();

        foreach ($data as $key => $item) {
            // Since uploaded file is removed/moved, we need to create a copy.

            $newFilePath = $this->projectDir . '/tests/resources' . '/' . $item['fileName'] . $key . $item['extension'];

            copy($this->projectDir . '/' . $item['filePath'], $newFilePath);

            $productFile = new ProductImportFile();
            $file = new UploadedFile(
                $newFilePath,
                $item['fileName'] . $item['extension'],
                null,
                null,
                true
            );

            $productFile->setFile($file);

            $manager->persist($productFile);
        }

        $manager->flush();
    }
}
