<?php

declare(strict_types=1);

namespace App\DataFixtures\Data;

final class ProductData
{
    /**
     * @return array[]
     */
    public static function getData(): array
    {
        return [
            [
                'uniqueId' => 99999999,
                'name' => 'Retro chair',
                'description' => 'lorem ipsum',
                'category' => null,
            ],
        ];
    }
}