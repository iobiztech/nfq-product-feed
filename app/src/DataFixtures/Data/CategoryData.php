<?php

declare(strict_types=1);

namespace App\DataFixtures\Data;

final class CategoryData
{
    /**
     * @return string[]
     */
    public static function getData(): array
    {
        return [
            [
                'name' => 'Test one',
            ],
            [
                'name' => 'Test two',
            ],
        ];
    }
}