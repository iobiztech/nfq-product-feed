<?php

declare(strict_types=1);

namespace App\DataFixtures\Data;

final class ProductImportFileData
{
    /**
     * @return string[][]
     */
    public static function getData(): array
    {
        return [
            [
                'filePath' => 'tests/resources/product_list_short.csv',
                'fileName' => 'test_file1',
                'extension' => '.csv',
            ],
        ];
    }
}