<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\ProductImportFile;

class ProductImportFileHelper
{
    /**
     * @param ProductImportFile|null $importFile
     *
     * @return bool
     */
    public function isFileSuitableForImport(?ProductImportFile $importFile): bool
    {
        if (!$importFile instanceof ProductImportFile) {
            return false;
        }

        if (is_null($importFile->getFile())) {
            return false;
        }

        if (!file_exists($importFile->getFile()->getPathname())) {
            return false;
        }

        return true;
    }
}
