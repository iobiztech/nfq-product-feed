<?php

declare(strict_types=1);

namespace App\Service\Manager;

use App\Repository\ProductRepository;

class ProductManager
{
    private ProductRepository $repository;

    /**
     * ProductManager constructor.
     *
     * @param ProductRepository $repository
     */
    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param int $externalId
     *
     * @return bool
     */
    public function isExistingProduct(int $externalId): bool
    {
        return $this->repository->count(['uniqueId' => $externalId]) > 0;
    }
}