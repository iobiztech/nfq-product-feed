<?php

declare(strict_types=1);

namespace App\Service\Manager;

use App\Entity\Category;
use App\Factory\CategoryFactory;
use App\Repository\CategoryRepository;

class CategoryManager
{
    private CategoryRepository $categoryRepository;

    /**
     * CategoryManager constructor.
     *
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param string $categoryName
     *
     * @return Category|null
     */
    public function getCategoryByName(string $categoryName): ?Category
    {
        $category = null;

        if (!empty($categoryName)) {
            $category = $this->categoryRepository->findOneBy(['name' => $categoryName]);
        }

        if (!empty($categoryName) && is_null($category)) {
            $category = CategoryFactory::create($categoryName);
        }

        return $category;
    }
}