<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Category;

class ProductCsvImporterHelper
{
    /**
     * @param array  $preImportedCategories
     * @param string $categoryName
     *
     * @return Category|null
     */
    public function getPreImportedCategoryOrNull(array $preImportedCategories, string $categoryName): ?Category
    {
        foreach ($preImportedCategories as $category) {
            if ($categoryName === $category->getName()) {
                return $category;
            }
        }

        return null;
    }
}
