<?php

declare(strict_types=1);

namespace App\Service\Importer\Product;

use App\Constant\Upload;
use App\Entity\Category;
use App\Entity\Product;
use App\Entity\ProductImportFile;
use App\Exception\ProductCsvImportException;
use App\Factory\ProductFactory;
use App\Service\Manager\CategoryManager;
use App\Service\Manager\ProductManager;
use App\Service\ProductCsvImporterHelper;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Throwable;

class CsvImporter implements ImporterInterface
{
    public const BATCH_SIZE = 20;

    public const VALID_TYPES = [
        Upload::MIME_TYPE_PLAIN,
        Upload::MIME_TYPE_CSV,
    ];

    private EntityManagerInterface $entityManager;
    private CategoryManager $categoryManager;
    private ProductCsvImporterHelper $csvImporterHelper;
    private ProductManager $productManager;
    private LoggerInterface $logger;

    /**
     * CsvImporter constructor.
     *
     * @param EntityManagerInterface   $entityManager
     * @param CategoryManager          $categoryManager
     * @param ProductCsvImporterHelper $csvImporterHelper
     * @param ProductManager           $productManager
     * @param LoggerInterface          $logger
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        CategoryManager $categoryManager,
        ProductCsvImporterHelper $csvImporterHelper,
        ProductManager $productManager,
        LoggerInterface $logger
    ) {
        $this->entityManager     = $entityManager;
        $this->categoryManager   = $categoryManager;
        $this->csvImporterHelper = $csvImporterHelper;
        $this->productManager    = $productManager;
        $this->logger            = $logger;
    }

    /**
     * @param ProductImportFile $importFile
     *
     * @return bool
     */
    public function isValid(ProductImportFile $importFile): bool
    {
        $file = $importFile->getFile();

        if (is_null($file)) {
            return false;
        }

        return in_array($file->getMimeType(), self::VALID_TYPES, true);
    }

    /**
     * @param ProductImportFile $importFile
     */
    public function execute(ProductImportFile $importFile): void
    {
        $this->entityManager->getConnection()->getConfiguration()->setSQLLogger(null);

        $i = 1;
        $handler = fopen($importFile->getFile()->getPathname(), 'r');
        $preImportedProductUniqueIds = $preImportedCategories = [];

        fgets($handler); // Column headers

        while (($row = fgetcsv($handler)) !== false) {
            try {
                $object = $this->getObjectToImport($row, $preImportedProductUniqueIds, $preImportedCategories);
            } catch (Throwable $exception) {
                $this->logger->error($exception->getMessage());

                continue;
            }

            $preImportedProductUniqueIds[] = $row[0];

            if ($object instanceof Category) {
                $preImportedCategories[] = $object;
            }

            $this->entityManager->persist($object);

            if (($i % self::BATCH_SIZE) === 0) {
                $this->entityManager->flush();
                $this->entityManager->clear();

                $preImportedProductUniqueIds = $preImportedCategories = [];
            }

            $i++;
        }

        fclose($handler);

        $this->entityManager->flush();
        $this->entityManager->clear();

        unset($preImportedProductUniqueIds, $preImportedCategories);
    }

    /**
     * @param array $row
     * @param array $preImportedProductUniqueIds
     * @param array $preImportedCategories
     *
     * @return Product|Category
     *
     * @throws ProductCsvImportException
     */
    private function getObjectToImport(array $row, array $preImportedProductUniqueIds, array $preImportedCategories)
    {
        [$uniqueId, $name, $categoryName, $description] = $row;

        if (empty($uniqueId)
            || $this->productManager->isExistingProduct((int) $uniqueId)
            || in_array($uniqueId, $preImportedProductUniqueIds, true)
        ) {
            throw new ProductCsvImportException(
                sprintf(
                    '%s: failed to import row. Empty or already existing uniqueId. [%s, %s, %s]',
                    self::class,
                    $name,
                    $categoryName,
                    $description
                )
            );
        }

        $category = $this->csvImporterHelper->getPreImportedCategoryOrNull($preImportedCategories, $categoryName);

        if (is_null($category)) {
            $category = $this->categoryManager->getCategoryByName($categoryName);
        }

        $product = ProductFactory::create((int) $uniqueId, $name, $description, $category);

        return !is_null($category) ? $category : $product;
    }
}
