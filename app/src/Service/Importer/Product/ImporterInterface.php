<?php

declare(strict_types=1);

namespace App\Service\Importer\Product;

use App\Entity\ProductImportFile;

interface ImporterInterface
{
    /**
     * @param ProductImportFile $importFile
     *
     * @return bool
     */
    public function isValid(ProductImportFile $importFile): bool;

    /**
     * @param ProductImportFile $importFile
     */
    public function execute(ProductImportFile $importFile): void;
}
