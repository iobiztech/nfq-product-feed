<?php

declare(strict_types=1);

namespace App\Service\Importer\Product;

use App\Entity\ProductImportFile;

class ProductImporter
{
    /**
     * @var iterable|ImporterInterface[]
     */
    private iterable $importers;

    /**
     * ProductImporter constructor.
     *
     * @param iterable $importers
     */
    public function __construct(iterable $importers)
    {
        $this->importers = $importers;
    }

    /**
     * @param ProductImportFile $importFile
     */
    public function import(ProductImportFile $importFile): void
    {
        foreach ($this->importers as $importer) {
            if (!$importer->isValid($importFile)) {
                continue;
            }

            $importer->execute($importFile);
        }
    }
}
