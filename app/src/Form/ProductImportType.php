<?php

declare(strict_types=1);

namespace App\Form;

use App\Constant\Upload;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use App\Entity\ProductImportFile;
use Vich\UploaderBundle\Form\Type\VichFileType;

class ProductImportType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('file', VichFileType::class, [
                'label' => 'form.csv_import.file.label',
                'constraints' => [
                    new File(['mimeTypes' => Upload::PRODUCT_IMPORT_VALID_MIME_TYPES,]),
                ],
                'attr' => [
                    'class' => 'form-control form-control-lg',
                ],
                'label_attr' => [
                    'class' => 'form-label',
                ],
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => ProductImportFile::class]);
    }
}
