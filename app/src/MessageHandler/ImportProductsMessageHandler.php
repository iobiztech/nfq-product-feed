<?php

declare(strict_types=1);

namespace App\MessageHandler;

use App\Entity\ProductImportFile;
use App\Message\ImportProductsMessage;
use App\Repository\ProductImportFileRepository;
use App\Service\Importer\Product\ProductImporter;
use App\Service\ProductImportFileHelper;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class ImportProductsMessageHandler implements MessageHandlerInterface
{
    private ProductImportFileRepository $importFileRepository;
    private ProductImportFileHelper $importFileHelper;
    private ProductImporter $productImporter;
    private LoggerInterface $logger;

    /**
     * ImportProductsMessageHandler constructor.
     *
     * @param ProductImportFileRepository $importFileRepository
     * @param ProductImportFileHelper     $importFileHelper
     * @param ProductImporter             $productImporter
     * @param LoggerInterface             $logger
     */
    public function __construct(
        ProductImportFileRepository $importFileRepository,
        ProductImportFileHelper $importFileHelper,
        ProductImporter $productImporter,
        LoggerInterface $logger
    ) {
        $this->importFileRepository = $importFileRepository;
        $this->importFileHelper     = $importFileHelper;
        $this->productImporter      = $productImporter;
        $this->logger               = $logger;
    }

    /**
     * @param ImportProductsMessage $message
     */
    public function __invoke(ImportProductsMessage $message)
    {
        /**
         * @var ProductImportFile|null $productImportFile
         */
        $productImportFile = $this->importFileRepository->find($message->getFileId());

        if (!$this->importFileHelper->isFileSuitableForImport($productImportFile)) {
            $this->logger->error(
                sprintf(
                    '%s failed to import products. Product file (ID: %d) is not valid or non existent.',
                    self::class,
                    $message->getFileId()
                )
            );

            return;
        }

        $this->productImporter->import($productImportFile);
    }
}
