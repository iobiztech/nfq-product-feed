<?php

declare(strict_types=1);

namespace App\Message;

final class ImportProductsMessage
{
    private int $fileId;

    /**
     * ImportProductsMessage constructor.
     *
     * @param int $fileId
     */
    public function __construct(int $fileId)
    {
        $this->fileId = $fileId;
    }

    /**
     * @return int
     */
    public function getFileId(): int
    {
        return $this->fileId;
    }
}
