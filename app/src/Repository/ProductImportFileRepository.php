<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\ProductImportFile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductImportFile|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductImportFile|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductImportFile[]    findAll()
 * @method ProductImportFile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductImportFileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductImportFile::class);
    }

    /**
     * @param ProductImportFile $file
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function create(ProductImportFile $file): void
    {
        $this->getEntityManager()->persist($file);
        $this->getEntityManager()->flush();
    }
}
