<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\Traits\TimestampableEntityTrait;
use App\Repository\ProductImportFileRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\Entity(repositoryClass=ProductImportFileRepository::class)
 * @Vich\Uploadable()
 * @ORM\HasLifecycleCallbacks
 */
class ProductImportFile
{
    use TimestampableEntityTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(name="file_name", type="string", nullable=false)
     *
     * @var string|null
     */
    private ?string $fileName = null;

    /**
     * @Vich\UploadableField(mapping="imports", fileNameProperty="fileName")
     *
     * @var File|null
     */
    private ?File $file = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    /**
     * @param string|null $fileName
     *
     * @return ProductImportFile
     */
    public function setFileName(?string $fileName): ProductImportFile
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * @return File|null
     */
    public function getFile(): ?File
    {
        return $this->file;
    }

    /**
     * @param File|null $file
     *
     * @return ProductImportFile
     */
    public function setFile(?File $file): ProductImportFile
    {
        $this->file = $file;

        if ($file instanceof File) {
            $this->updatedAt = new DateTimeImmutable();
        }

        return $this;
    }
}
