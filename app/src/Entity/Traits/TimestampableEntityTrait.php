<?php

declare(strict_types=1);

namespace App\Entity\Traits;

use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

trait TimestampableEntityTrait
{
    /**
     * @var DateTimeInterface|null
     *
     * @ORM\Column(name="created_at", type="datetime_immutable", nullable=false)
     */
    protected ?DateTimeInterface $createdAt = null;

    /**
     * @var DateTimeInterface|null
     *
     * @ORM\Column(name="updated_at", type="datetime_immutable", nullable=false)
     */
    protected ?DateTimeInterface $updatedAt = null;

    /**
     * @return DateTimeInterface
     */
    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt ?? new DateTimeImmutable();
    }

    /**
     * @param DateTimeInterface $createdAt
     *
     * @return self
     */
    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return DateTimeInterface
     */
    public function getUpdatedAt(): DateTimeInterface
    {
        return $this->updatedAt ?? new DateTimeImmutable();
    }

    /**
     * @param DateTimeInterface $updatedAt
     *
     * @return self
     */
    public function setUpdatedAt(DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate(): void
    {
        $this->setUpdatedAt(new DateTimeImmutable());
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist(): void
    {
        $now = new DateTimeImmutable();

        $this->setCreatedAt($now);
        $this->setUpdatedAt($now);
    }
}
