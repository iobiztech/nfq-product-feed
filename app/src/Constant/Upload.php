<?php

declare(strict_types=1);

namespace App\Constant;

final class Upload
{
    public const MIME_TYPE_CSV = 'text/csv';
    public const MIME_TYPE_PLAIN = 'text/plain';

    public const PRODUCT_IMPORT_VALID_MIME_TYPES = [
        self::MIME_TYPE_CSV,
        self::MIME_TYPE_PLAIN,
    ];
}
