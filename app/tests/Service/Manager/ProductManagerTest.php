<?php

declare(strict_types=1);

namespace App\Tests\Service\Manager;

use App\Service\Manager\ProductManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ProductManagerTest extends KernelTestCase
{
    /**
     * @dataProvider provideData
     *
     * @param int  $uniqueId
     * @param bool $expectedResult
     */
    public function testIsExistingProduct(int $uniqueId, bool $expectedResult): void
    {
        self::bootKernel();

        /**
         * @var ProductManager $productManager
         */
        $productManager = static::getContainer()->get(ProductManager::class);

        self::assertEquals($expectedResult, $productManager->isExistingProduct($uniqueId));
    }

    /**
     * @return array[]
     */
    public static function provideData(): array
    {
        return [
            [99999999, true,],
            [1231312312313, false,],
        ];
    }
}