<?php

declare(strict_types=1);

namespace App\Tests\Service\Manager;

use App\Entity\Category;
use App\Service\Manager\CategoryManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CategoryManagerTest extends KernelTestCase
{
    /**
     * @dataProvider provideCategories
     */
    public function testGetCategoryByName(string $name, ?string $result): void
    {
        self::bootKernel();

        /**
         * @var CategoryManager $categoryManager
         */
        $categoryManager = static::getContainer()->get(CategoryManager::class);
        $categoryByName  = $categoryManager->getCategoryByName($name);

        $this->assertEquals($result, !is_null($categoryByName) ? get_class($categoryByName) : $categoryByName);
    }

    /**
     * @return array
     */
    public function provideCategories(): array
    {
        return [
            ['Test one', Category::class],
            ['Not existing name', Category::class],
            ['', null],
        ];
    }
}
