<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Entity\Category;
use App\Exception\ProductCsvImportException;
use App\Service\ProductCsvImporterHelper;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ProductCsvImporterHelperTest extends KernelTestCase
{
    /**
     *
     */
    public function testGetPreImportedCategoryOrNullOnEmptyArray(): void
    {
        self::bootKernel();

        /**
         * @var ProductCsvImporterHelper $productCsvImporterHelper
         */
        $productCsvImporterHelper = static::getContainer()->get(ProductCsvImporterHelper::class);
        $emptyArray = [];

        self::assertNull($productCsvImporterHelper->getPreImportedCategoryOrNull($emptyArray, 'test'));
    }

    /**
     *
     */
    public function testGetPreImportedCategoryOrNullWithoutRequiredCategory(): void
    {
        self::bootKernel();

        /**
         * @var ProductCsvImporterHelper $productCsvImporterHelper
         */
        $productCsvImporterHelper = static::getContainer()->get(ProductCsvImporterHelper::class);

        $emptyArray = [];
        $emptyArray[] = (new Category())->setName('Test one');
        $emptyArray[] = (new Category())->setName('Test two');
        $emptyArray[] = (new Category())->setName('Test three');

        self::assertNull($productCsvImporterHelper->getPreImportedCategoryOrNull($emptyArray, 'Test four'));
    }

    /**
     *
     */
    public function testGetPreImportedCategoryOrNullWithRequiredCategory(): void
    {
        self::bootKernel();

        /**
         * @var ProductCsvImporterHelper $productCsvImporterHelper
         */
        $productCsvImporterHelper = static::getContainer()->get(ProductCsvImporterHelper::class);

        $emptyArray = [];
        $emptyArray[] = (new Category())->setName('Test one');
        $emptyArray[] = (new Category())->setName('Test two');
        $emptyArray[] = (new Category())->setName('Test three');
        $emptyArray[] = (new Category())->setName('Test four');

        $requiredCategory = $productCsvImporterHelper->getPreImportedCategoryOrNull($emptyArray, 'Test four');

        self::assertInstanceOf(Category::class, $requiredCategory);
        self::assertEquals('Test four', $requiredCategory->getName());
    }
}
