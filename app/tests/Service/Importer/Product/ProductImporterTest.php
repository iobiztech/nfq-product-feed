<?php

declare(strict_types=1);

namespace App\Tests\Service\Importer\Product;

use App\Entity\ProductImportFile;
use App\Repository\ProductRepository;
use App\Service\Importer\Product\ProductImporter;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\File\File;

class ProductImporterTest extends KernelTestCase
{
    public const PATH_TO_RESOURCES = '/tests/resources/';

    private ProductRepository $productRepository;
    private ProductImporter $productImporter;
    private string $projectDir;

    /**
     *
     */
    public function setUp(): void
    {
        $this->productRepository = static::getContainer()->get(ProductRepository::class);
        $this->productImporter = static::getContainer()->get(ProductImporter::class);
        $this->projectDir = static::getContainer()->getParameter('kernel.project_dir');
    }

    /**
     * @dataProvider provideData
     *
     * @param string $fileName
     * @param int    $increaseOfProducts
     */
    public function testImport(string $fileName, int $increaseOfProducts): void
    {
        self::bootKernel();

        $productCountBefore = $this->productRepository->count([]);
        $expectedCount = $productCountBefore + $increaseOfProducts;

        $file = new File($this->projectDir . self::PATH_TO_RESOURCES . $fileName);

        $importFile = new ProductImportFile();
        $importFile->setFile($file);

        $this->productImporter->import($importFile);

        $productCountAfter = $this->productRepository->count([]);

        self::assertEquals($expectedCount, $productCountAfter);
    }

    /**
     * @return array[]
     */
    public static function provideData(): array
    {
        return [
            ['product_list_of_all_valid.csv', 3,],
            ['product_list_of_no_valid.csv', 0,],
            ['product_list_of_one_valid.csv', 1,],
        ];
    }
}
