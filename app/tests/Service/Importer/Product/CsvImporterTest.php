<?php

declare(strict_types=1);

namespace App\Tests\Service\Importer\Product;

use App\Entity\ProductImportFile;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use App\Service\Importer\Product\CsvImporter;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\File\File;

class CsvImporterTest extends KernelTestCase
{
    public const PATH_TO_RESOURCES = '/tests/resources/';

    private ProductRepository $productRepository;
    private CsvImporter $csvImporter;
    private string $projectDir;
    private CategoryRepository $categoryRepository;

    /**
     *
     */
    public function setUp(): void
    {
        $this->productRepository = static::getContainer()->get(ProductRepository::class);
        $this->categoryRepository = static::getContainer()->get(CategoryRepository::class);
        $this->csvImporter = static::getContainer()->get(CsvImporter::class);
        $this->projectDir = static::getContainer()->getParameter('kernel.project_dir');
    }

    /**
     * @group importer
     *
     * @dataProvider provideData
     *
     * @param string $fileName
     * @param int    $increaseOfProducts
     */
    public function testExecute(string $fileName, int $increaseOfProducts): void
    {
        self::bootKernel();

        $productCountBefore = $this->productRepository->count([]);
        $expectedCount = $productCountBefore + $increaseOfProducts;

        $file = new File($this->projectDir . self::PATH_TO_RESOURCES . $fileName);

        $importFile = new ProductImportFile();
        $importFile->setFile($file);

        self::assertTrue($this->csvImporter->isValid($importFile));

        $this->csvImporter->execute($importFile);

        $productCountAfter = $this->productRepository->count([]);

        self::assertEquals($expectedCount, $productCountAfter);
    }

    /**
     * @group importFix
     *
     * @dataProvider provideDuplicateData
     */
    public function testImportWithCategoryDuplicates(string $fileName, string $categoryName): void
    {
        self::bootKernel();

        $file = new File($this->projectDir . self::PATH_TO_RESOURCES . $fileName);

        $importFile = new ProductImportFile();
        $importFile->setFile($file);

        self::assertTrue($this->csvImporter->isValid($importFile));

        $this->csvImporter->execute($importFile);

        $countAfter = $this->categoryRepository->count(['name' => $categoryName,]);

        self::assertEquals(1, $countAfter);
    }

    /**
     * @return array[]
     */
    public static function provideData(): array
    {
        return [
            ['product_list_of_all_valid.csv', 3,],
            ['product_list_of_no_valid.csv', 0,],
            ['product_list_of_one_valid.csv', 1,],
        ];
    }

    /**
     * @return array
     */
    public static function provideDuplicateData(): array
    {
        return [
            ['product_list_category_duplicates.csv', 'Doig',],
        ];
    }
}
