<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Entity\ProductImportFile;
use App\Service\ProductImportFileHelper;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\File\File;

class ProductImportFileHelperTest extends KernelTestCase
{
    /**
     *
     */
    public function testIsFileSuitableForImportWithFileNull(): void
    {
        self::bootKernel();

        $productImportFileHelper = static::getContainer()->get(ProductImportFileHelper::class);
        $productImportFile = new ProductImportFile();

        $this->assertFalse($productImportFileHelper->isFileSuitableForImport($productImportFile));
    }

    /**
     *
     */
    public function testIsFileSuitableForImportWithProductImportFileAsNull(): void
    {
        self::bootKernel();

        $productImportFileHelper = static::getContainer()->get(ProductImportFileHelper::class);

        $this->assertFalse($productImportFileHelper->isFileSuitableForImport(null));
    }

    /**
     *
     */
    public function testIsFileSuitableForImportWithNonExistentFile(): void
    {
        self::bootKernel();

        $productImportFileHelper = static::getContainer()->get(ProductImportFileHelper::class);
        $productImportFile = new ProductImportFile();

        $file = new File(self::$kernel->getProjectDir() . '/tests/resources/product_list_non_existent.csv', false);
        $productImportFile->setFile($file);

        $this->assertFalse($productImportFileHelper->isFileSuitableForImport($productImportFile));
    }
}
