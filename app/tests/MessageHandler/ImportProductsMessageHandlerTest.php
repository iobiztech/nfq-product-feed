<?php

declare(strict_types=1);

namespace App\Tests\MessageHandler;

use App\Message\ImportProductsMessage;
use App\MessageHandler\ImportProductsMessageHandler;
use App\Repository\ProductImportFileRepository;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ImportProductsMessageHandlerTest extends KernelTestCase
{
    /**
     * @group messenger
     */
    public function testImportHandlerWithExistingFile(): void
    {
        self::bootKernel();

        /**
         * @var ImportProductsMessageHandler $handler
         */
        $handler = static::getContainer()->get(ImportProductsMessageHandler::class);

        /**
         * @var ProductImportFileRepository $importFileRepo
         */
        $importFileRepo = static::getContainer()->get(ProductImportFileRepository::class);

        /**
         * @var ProductRepository $productRepo
         */
        $productRepo = static::getContainer()->get(ProductRepository::class);

        $importFile = $importFileRepo->findAll()[0];

        $productCountBefore = $productRepo->count([]);
        $expectedResult = $productCountBefore + 3;

        $handler(new ImportProductsMessage($importFile->getId()));

        $productCountAfter = $productRepo->count([]);

        self::assertEquals($expectedResult, $productCountAfter);
    }

    /**
     * @group messenger
     */
    public function testImportHandlerWithNonExistentImportFileId(): void
    {
        self::bootKernel();

        /**
         * @var ImportProductsMessageHandler $handler
         */
        $handler = static::getContainer()->get(ImportProductsMessageHandler::class);

        /**
         * @var ProductRepository $productRepo
         */
        $productRepo = static::getContainer()->get(ProductRepository::class);


        $productCountBefore = $productRepo->count([]);

        $handler(new ImportProductsMessage(120000));

        $productCountAfter = $productRepo->count([]);

        self::assertEquals($productCountBefore, $productCountAfter);
    }
}
