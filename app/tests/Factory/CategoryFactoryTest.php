<?php

declare(strict_types=1);

namespace App\Tests\Factory;

use App\Entity\Category;
use App\Factory\CategoryFactory;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CategoryFactoryTest extends KernelTestCase
{
    /**
     * @group factory
     *
     * @param string      $value
     * @param string|null $expectedResult
     *
     * @dataProvider provideCategoryData
     */
    public function testCreate(string $value, ?string $expectedResult): void
    {
        self::bootKernel();

        $factory = new CategoryFactory();
        $objectCreatedByFactory = $factory::create($value);

        $this->assertEquals(
            $expectedResult,
            !is_null($objectCreatedByFactory) ? get_class($objectCreatedByFactory) : $objectCreatedByFactory
        );
    }

    /**
     * @return array
     */
    public static function provideCategoryData(): array
    {
        return [
            ['', null,],
            ['test', Category::class,],
        ];
    }
}