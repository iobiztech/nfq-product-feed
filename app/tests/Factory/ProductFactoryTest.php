<?php

declare(strict_types=1);

namespace App\Tests\Factory;

use App\Entity\Category;
use App\Factory\ProductFactory;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ProductFactoryTest extends KernelTestCase
{
    /**
     * @group factory
     * @dataProvider provideData
     *
     * @param int           $uniqueId
     * @param string|null   $name
     * @param string|null   $description
     * @param Category|null $category
     */
    public function testCreate(int $uniqueId, ?string $name, ?string $description, ?Category $category): void
    {
        self::bootKernel();

        $factory = new ProductFactory();
        $product = $factory::create($uniqueId, $name, $description, $category);

        self::assertIsInt($product->getUniqueId());

        if (is_null($name)) {
            self::assertNull($product->getName());
        }

        if (empty($name)) {
            self::assertNull($product->getName());
        }

        if (is_null($description)) {
            self::assertNull($product->getDescription());
        }

        if (empty($description)) {
            self::assertNull($product->getDescription());
        }

        if (is_null($category)) {
            self::assertNull($product->getCategory());
        }

        if ($category instanceof Category) {
            self::assertTrue($category->getProducts()->contains($product));
        }
    }

    /**
     * @return array[]
     */
    public static function provideData(): array
    {
        return [
            [
                1,
                'Table',
                'Lorem ipsum',
                (new Category())->setName('Furniture'),
            ],
            [
                2,
                null,
                '',
                (new Category())->setName('Lights'),
            ],
            [
                3,
                '',
                '',
                null,
            ],
            [
                4,
                null,
                null,
                null,
            ],
        ];
    }
}