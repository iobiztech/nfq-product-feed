<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Messenger\Transport\InMemoryTransport;

class ProductControllerTest extends WebTestCase
{
    /**
     * @group smokeTest
     *
     * @dataProvider provideRoutes
     */
    public function testIsSuccessfulResponse(string $route, int $expectedStatus): void
    {
        $client = self::createClient();
        $client->request('GET', str_replace(' ', '%20', $route));

        self::assertEquals($expectedStatus, $client->getResponse()->getStatusCode());
    }

    /**
     * @group messenger
     */
    public function testImportActionWithValidFile(): void
    {
        $client = static::createClient();

        $projectDir   = $client->getContainer()->getParameter('kernel.project_dir');
        $crawler      = $client->request('GET', '/');
        $submitButton = $crawler->selectButton('Import');

        $form = $submitButton->form();
        $form['product_import[file][file]']->upload($projectDir . '/tests/resources/product_list_short.csv');

        $client->submit($form);

        /**
         * @var InMemoryTransport $transport
         */
        $transport = $client->getContainer()->get('messenger.transport.async');

        self::assertCount(1, $transport->get());

        $client->followRedirect();

        self::assertResponseIsSuccessful();

        $transport->reset();
    }

    /**
     * @group messenger
     */
    public function testImportActionWithInvalidFile(): void
    {
        $client = static::createClient();

        /**
         * @var InMemoryTransport $transport
         */
        $transport = $client->getContainer()->get('messenger.transport.async');

        $transport->reset();

        $projectDir   = $client->getContainer()->getParameter('kernel.project_dir');
        $crawler      = $client->request('GET', '/');
        $submitButton = $crawler->selectButton('Import');
        $form = $submitButton->form();
        $form['product_import[file][file]']->upload($projectDir . '/tests/resources/product_list_short.docx');

        $client->submit($form);

        self::assertCount(0, $transport->get());
    }

    /**
     * @group messenger
     */
    public function testImportActionWithNoFile(): void
    {
        $client = static::createClient();

        /**
         * @var InMemoryTransport $transport
         */
        $transport = $client->getContainer()->get('messenger.transport.async');

        $transport->reset();

        $crawler      = $client->request('GET', '/');
        $submitButton = $crawler->selectButton('Import');
        $form = $submitButton->form();

        $client->submit($form);

        self::assertCount(0, $transport->get());
    }

    /**
     *
     */
    public function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @return array[]
     */
    public static function provideRoutes(): array
    {
        return [
            ['/', 200,],
            ['/categories', 200,],
            ['/categories/Test one', 200,],
            ['/categories/not exists', 404,],
        ];
    }
}