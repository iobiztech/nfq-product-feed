# Specification

- Php 7.4
- Mysql 8
- Symfony 5.3
- Yarn 1.22.5
- Node v15.14.0

# Installation

1. cd app
2. cp .env.dist .env
3. Run the containers: ``docker-compose up -d``
4. Get inside to container: ``docker-compose exec php-service bash``
5. Install dependencies: ``composer install``
6. Exit container: ``exit``
7. Install assets: ``docker-compose run --rm node-service yarn encore dev``
8. Enter container again: ``docker-compose exec php-service bash``
8. Create database: ``php bin/console doctrine:database:create``
9. Run migrations: ``php bin/console doctrine:migrations:migrate``
10. Run messenger worker: ``php bin/console messenger:consume async -vv``
11. Done.

# Tests

1. Get inside to container: ``docker-compose exec php-service bash``
2. Create testing database: ``php bin/console doctrine:database:create --env=test``
3. Update schema: ``php bin/console doctrine:schema:create --force --env=test``
4. Run fixtures: ``php bin/console doctrine:fixtures:load --env=test``
5. Run: ``php bin/phpunit``


## Get inside to container: 
``docker-compose exec php-service bash``

## Run container: 
``docker-compose up -d``